SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `contaModel`;
CREATE TABLE `contaModel` (
  `id` varchar(25) NOT NULL,
  `saldo` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `transferenciaInput`;
CREATE TABLE `transferenciaInput` (
  `id` int NOT NULL,
  `id_conta_origem` int NOT NULL,
  `id_conta_destino` int NOT NULL,
  `valor` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;