package br.com.djheison.hexagonalarchitecture.adapter.persistence.transferencia;

import br.com.djheison.hexagonalarchitecture.core.domain.transferencia.Conta;
import br.com.djheison.hexagonalarchitecture.core.service.transferencia.out.SalvaContaBancariaPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class SalvaContaBancaria implements SalvaContaBancariaPort {

    private final ContaBancariaRepository contaBancariaRepository;

    @Override
    public Conta save(Conta conta) {
        return ContaBancariaMapper.toDomain(
                contaBancariaRepository.save(ContaBancariaMapper.toModel(conta)));
    }
}