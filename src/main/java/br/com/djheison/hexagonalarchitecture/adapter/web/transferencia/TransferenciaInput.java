package br.com.djheison.hexagonalarchitecture.adapter.web.transferencia;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class TransferenciaInput {

    @NotNull
    @JsonProperty("idContaOrigem")
    private Long idContaOrigem;

    @NotNull
    @JsonProperty("idContaDestino")
    private Long idContaDestino;

    @NotNull
    @JsonProperty("valor")
    private BigDecimal valor;
}
