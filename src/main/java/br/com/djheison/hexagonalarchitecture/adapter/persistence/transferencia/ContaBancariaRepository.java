package br.com.djheison.hexagonalarchitecture.adapter.persistence.transferencia;

import org.springframework.data.repository.CrudRepository;

public interface ContaBancariaRepository extends CrudRepository<ContaModel, Long> {

}
