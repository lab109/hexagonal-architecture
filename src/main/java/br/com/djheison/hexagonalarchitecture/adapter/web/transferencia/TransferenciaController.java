package br.com.djheison.hexagonalarchitecture.adapter.web.transferencia;

import br.com.djheison.hexagonalarchitecture.core.service.transferencia.in.TransferenciaBancariaUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/v1/transferencia",
        consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class TransferenciaController {

    private final TransferenciaBancariaUseCase transferenciaBancariaUseCase;

    @GetMapping
    public ResponseEntity transferir(@RequestBody @Valid TransferenciaInput transferenciaInput) {
        transferenciaBancariaUseCase.transferir(transferenciaInput.getIdContaOrigem(),
                transferenciaInput.getIdContaDestino(), transferenciaInput.getValor());
        return ResponseEntity.ok().build();
    }
}
