package br.com.djheison.hexagonalarchitecture.adapter.persistence.transferencia;

import br.com.djheison.hexagonalarchitecture.core.domain.transferencia.Conta;
import br.com.djheison.hexagonalarchitecture.core.service.transferencia.out.BuscaContaBancariaPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Log4j2
@Component
@RequiredArgsConstructor
public class BuscaContaBancaria implements BuscaContaBancariaPort {

    private final ContaBancariaRepository contaBancariaRepository;

    @Override
    public Optional<Conta> findById(Long idConta) {
        return contaBancariaRepository.findById(idConta)
                .map(ContaBancariaMapper::toDomain);
    }
}
