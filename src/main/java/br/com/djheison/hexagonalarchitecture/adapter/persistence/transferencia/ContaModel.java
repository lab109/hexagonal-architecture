package br.com.djheison.hexagonalarchitecture.adapter.persistence.transferencia;

import lombok.Builder;

import javax.persistence.*;
import java.math.BigDecimal;

@Builder
@Entity
@Table(name = "conta")
public class ContaModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "saldo")
    private BigDecimal saldo;

    public ContaModel() {
    }

    public ContaModel(Long id, BigDecimal saldo) {
        this.id = id;
        this.saldo = saldo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }
}
