package br.com.djheison.hexagonalarchitecture.adapter.persistence.transferencia;

import br.com.djheison.hexagonalarchitecture.core.domain.transferencia.Conta;

public class ContaBancariaMapper {

    static Conta toDomain(ContaModel contaModel) {
        return Conta.builder()
                .id(contaModel.getId())
                .saldo(contaModel.getSaldo())
                .build();
    }

    static ContaModel toModel(Conta conta) {
        return ContaModel.builder()
                .id(conta.getId())
                .saldo(conta.getSaldo())
                .build();
    }
}
