package br.com.djheison.hexagonalarchitecture.core.domain.transferencia;

import lombok.AllArgsConstructor;

import java.math.BigDecimal;

@AllArgsConstructor
public class Transferencia {

    private Conta contaOrigem;
    private Conta contaDestino;
    private BigDecimal valor;

    public void transferir() {
        contaOrigem.debitar(this.valor);
        contaDestino.creditar(this.valor);
    }
}
