package br.com.djheison.hexagonalarchitecture.core.service.transferencia.in;

import java.math.BigDecimal;

public interface TransferenciaBancariaUseCase {
    void transferir(Long idContaOrigem, Long idContaDestino, BigDecimal valor);
}
