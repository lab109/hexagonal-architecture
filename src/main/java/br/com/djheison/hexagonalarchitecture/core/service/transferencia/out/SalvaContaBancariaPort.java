package br.com.djheison.hexagonalarchitecture.core.service.transferencia.out;

import br.com.djheison.hexagonalarchitecture.core.domain.transferencia.Conta;

public interface SalvaContaBancariaPort {

    Conta save(Conta conta);
}
