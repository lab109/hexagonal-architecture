package br.com.djheison.hexagonalarchitecture.core.service.transferencia;

import br.com.djheison.hexagonalarchitecture.core.domain.transferencia.Conta;
import br.com.djheison.hexagonalarchitecture.core.domain.transferencia.Transferencia;
import br.com.djheison.hexagonalarchitecture.core.service.transferencia.in.TransferenciaBancariaUseCase;
import br.com.djheison.hexagonalarchitecture.core.service.transferencia.out.BuscaContaBancariaPort;
import br.com.djheison.hexagonalarchitecture.core.service.transferencia.out.SalvaContaBancariaPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.util.Optional;


@Log4j2
@Named
@RequiredArgsConstructor(onConstructor=@__(@Inject))
public class TransferenciaBancaria implements TransferenciaBancariaUseCase {

    private final BuscaContaBancariaPort contaBancariaPort;
    private final SalvaContaBancariaPort salvaContaBancariaPort;

    @Override
    public void transferir(Long idContaOrigem, Long idContaDestino, BigDecimal valor) {
        try {
            Optional<Conta> contaOrigem = contaBancariaPort.findById(idContaOrigem);
            Optional<Conta> contaDestino = contaBancariaPort.findById(idContaDestino);
            new Transferencia(contaOrigem.get(), contaDestino.get(), valor).transferir();
            salvaContaBancariaPort.save(contaOrigem.get());
            salvaContaBancariaPort.save(contaDestino.get());
        }
        catch (Exception e) {
            log.error("Erro ao realizar transferência {}", e);
            throw e;
        }
    }
}
