package br.com.djheison.hexagonalarchitecture.core.service.transferencia.out;

import br.com.djheison.hexagonalarchitecture.core.domain.transferencia.Conta;

import java.util.Optional;

public interface BuscaContaBancariaPort {

    Optional<Conta> findById(Long idConta);
}
